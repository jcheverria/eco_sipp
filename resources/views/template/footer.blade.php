   <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <img style="display: block;margin-left: auto;margin-right: auto;" width="250" height="52" src="https://ecomundo.edu.ec/np/wp-content/uploads/2017/03/logo_ecomundo_white_250.png" class="custom-logo" alt="" itemprop="logo">
                       <p>{{config('notika.footer-text')}}</p>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>