<?php


return [

      'title'   =>  'ECO SIP',

      // show or hide default widget.
      // default widget can be found in header-widgets.blade.php

      'default-header-widgets'    => true,

      'icon'      => 'https://ecomundo.edu.ec/np/wp-content/uploads/2017/03/logo_ecomundo_16.png',

      'footer-text' => 'Copyright © 2019 . All rights reserved. ECOMUNDO.',

      // add target for nested menu

      'menus'    =>  [
            [
                  'text' =>   'Home',
                  'url'  =>   '',
                  'icon' =>   'notika-house',
            ],
            [
                  'text' => 'Gestión de estudiante',
                  'icon' => 'notika-app',
                  'target' => 'menu-befores',
                  'url' => [
                        'dataaaa',
                        'data',
                  ],
                  'nested' => [

                        [
                              'text' => 'sub menu 1',
                        ],
                        [
                              'text' => 'sub menu 2',
                        ],
                  ],
            ],

            [
                  'text' => 'Gestión de evaluacion',
                  'icon' => 'notika-draft',
                  'target' => 'menu-after',
                  'url' => [
                        'ca'
                  ],
                  'nested' => [
                        [
                              'text' => 'sub menu 1',
                        ],
                  ],
            ],

            [
                  'text' => 'Evaluación',
                  'icon' => 'notika-draft',
                  'target' => 'menu-before',
                  'url' => [
                        'ca'
                  ],
                  'nested' => [
                        [
                              'text' => 'sub menu 1',
                        ],
                  ],
            ],
            [
                  'text' => 'Configuración de algoritmo',
                  'icon' => 'notika-draft',
                  'target' => 'menu-sidebar',
                  'url' => [
                        'ca'
                  ],
                  'nested' => [
                        [
                              'text' => 'sub menu 1',
                        ],
                  ],
            ],

      ],
];



// .notika-menu-befores

// .notika-menu-after

// .notika-menu-before

// .notika-menu-sidebar

// .notika-skype

// .notika-app

// .notika-form

// .notika-windows

// .notika-bar-chart

// .notika-alarm

// .notika-arrow-right

// .notika-avable

// .notika-back

// .notika-calendar

// .notika-chat

// .notika-checked

// .notika-close

// .notika-cloud

// .notika-credit-card

// .notika-dollar

// .notika-dot

// .notika-down-arrow

// .notika-draft

// .notika-edit

// .notika-eye

// .notika-facebook

// .notika-file

// .notika-finance

// .notika-flag

// .notika-house

// .notika-ip-locator

// .notika-left-arrow

// .notika-mail

// .notika-map

// .notika-menu

// .notika-menus

// .notika-minus-symbol

// .notika-more-button

// .notika-next

// .notika-next-pro

// .notika-paperclip

// .notika-phone

// .notika-picture

// .notika-pinterest

// .notika-plus-symbol

// .notika-print

// .notika-promos

// .notika-refresh

// .notika-right-arrow

// .notika-search

// .notika-sent

// .notika-settings

// .notika-social

// .notika-star

// .notika-success

// .notika-support

// .notika-tax

// .notika-trash

// .notika-travel

// .notika-twitter

// .notika-up-arrow

// .notika-wifi
